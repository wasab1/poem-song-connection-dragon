# 诗-歌-接-龙

#### 介绍
本系统是一个多功能的诗词学习系统，实现了诗词搜索、诗歌接龙、词云显示三种功能。其中诗词搜索功能可以通过用户输入的标题、朝代、作者和诗词内容等关键词返回对应结果，通过TF-IDF和BM25F自定义方法使关联度更高的结果优先显示；基于pypinyin、Aip和playsound实现诗歌接龙并朗读接龙内容的功能。词云显示基于jieba库的中文分词技术以及WordCloud库的词云制作技术生成的词云图片。

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
