# 诗-歌-接-龙

#### Description
本系统是一个多功能的诗词学习系统，实现了诗词搜索、诗歌接龙、词云显示三种功能。其中诗词搜索功能可以通过用户输入的标题、朝代、作者和诗词内容等关键词返回对应结果，通过TF-IDF和BM25F自定义方法使关联度更高的结果优先显示；基于pypinyin、Aip和playsound实现诗歌接龙并朗读接龙内容的功能。词云显示基于jieba库的中文分词技术以及WordCloud库的词云制作技术生成的词云图片。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
