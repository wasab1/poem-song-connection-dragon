import re
import jieba
from PIL import Image#导入PIL模块处理图片
from wordcloud import WordCloud #导入词云模块
words = open('孟浩然内容.txt',encoding='utf-8').read()#打开歌词文件，获取到歌词
new_words = ' '.join(jieba.cut(words))#使用jieba.cut分词，然后把分好的词变成一个字符串，每个词用空格隔开
new_words=(re.sub('[之而兮也不与有为我是]', '', new_words))#去掉所有诗歌的前十高频词
wordcloud = WordCloud(width=1000, #图片的宽度
                      height=860,  #高度
                      margin=2, #边距
                      # mask=alice_mask,
                      background_color='#F0F8FF',#指定背景颜色，这里用的是颜色代码
                      # font_path='D:\字体\百度综艺简体.ttf'#指定字体文件
                        font_path = 'D:\字体\字魂55号-龙吟手书.ttf'
                      )
wordcloud.generate(new_words) #分词
wordcloud.to_file('孟浩然词云.png')#保存到图片