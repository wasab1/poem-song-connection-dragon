from tkinter import *
from PIL import Image, ImageTk
from test import test1, test2, test3
from mySearch import Searching
from playAudio import play
import sys
sys.path.append('/2-pypinyin/test.py')

class MainMenu2(object):
    def __init__(self, master=None):
        self.root = master  # 定义内部变量root
        self.root.geometry('%dx%d' % (800, 800))  # 设置窗口大小
        self.createPage()

    def createPage(self):
        self.searchPage = SearchFrame(self.root)  # 创建不同Frame
        self.jielongPage = JielongFrame(self.root)
        self.ciyunPage = CiyunFrame(self.root)
        self.searchPage.pack()  # 默认搜索界面
        menubar = Menu(self.root)
        menubar.add_command(label='诗词搜索', font=('黑体', 15, 'bold'),command=self.search)
        menubar.add_command(label='诗词接龙', font=('黑体', 15, 'bold'), command=self.jielong)
        menubar.add_command(label='词云',  font=('黑体', 15, 'bold'),command=self.ciyun)
        self.root['menu'] = menubar  # 设置菜单栏

    def search(self):
        self.searchPage.pack()
        self.jielongPage.pack_forget()
        self.ciyunPage.pack_forget()

    def jielong(self):
        self.searchPage.pack_forget()
        self.jielongPage.pack()
        self.ciyunPage.pack_forget()

    def ciyun(self):
        self.searchPage.pack_forget()
        self.jielongPage.pack_forget()
        self.ciyunPage.pack()

class SearchFrame(Frame):  # 继承Frame类
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.root = master  # 定义内部变量root
        self.enter = StringVar()
        self.createPage()
        self.search_Text = Text(self, width=125, height=32,font=('楷体', 13, 'bold'))  # 日志框
        self.search_Text.grid(row=13, column=0, columnspan=10)


    def createPage(self):
        # self.page = Frame(self.root)  # 创建Frame
        Label(self, text='诗词搜索', font=('黑体', 20, 'bold')).grid(row=0, column=0)
        # Label(self,text='请输入： ',font=('黑体', 15, 'bold')).grid(row=1,column=1,stick=W)#W是靠左
        # Entry(self,width=50).grid(row=1,column=1)
        Entry(self, width=150,textvariable=self.enter).grid(row=2, column=0)
        Button(self,text='搜索',font=('黑体', 12, 'bold'),command=self.searching).grid(row=2,column=1,stick=E)

    def searching(self):
        self.search_Text.delete(1.0,END)
        data = self.enter.get()
        # self.write_to_Text(data)
        list = Searching(data)
        for i in list:
            self.write_to_Text("\n")
            self.write_to_Text("                                                        "+i['title'])
            self.write_to_Text("                                                          "+i['authors'])
            self.write_to_Text("                                                          "+i['dynasties'])
            self.write_to_Text(i['contents'])
            self.write_to_Text("\n")
            self.write_to_Text("=============================================================================================================================")
            self.write_to_Text("=============================================================================================================================")

    def write_to_Text(self,logmsg):
        logmsg_in = str(logmsg) + "\n"      #换行
        self.search_Text.insert(END, logmsg_in)


class JielongFrame(Frame):  # 继承Frame类
    def __init__(self,master=None):
        Frame.__init__(self,master)
        self.root = master  # 定义内部变量root
        self.createPage()
        self.enter = StringVar()
        # self.enter2 = StringVar()
        self.jielong_Text = Text(self, width=125, height=32,font=('楷体', 13, 'bold'))
        self.jielong_Text.grid(row=13, column=0, columnspan=10)


    def createPage(self):
        Label(self,text='诗词接龙', font=('黑体', 20, 'bold')).grid(row=0,column=4)
        Button(self,text='简单模式',font=('黑体', 15, 'bold'),command=self.print1).grid(row=1,column=2)
        Button(self,text='普通模式',font=('黑体', 15, 'bold'),command=self.print2).grid(row=1,column=4)
        Button(self,text='困难模式',font=('黑体', 15, 'bold'),command=self.print3).grid(row=1,column=6)
        # Label(self, text='请输入诗词：', font=('黑体', 15, 'bold')).grid(row=2, column=2,stick=W)
        # Text(self).grid(row=4,column=2,columnspan=5)
    def print1(self):
        self.jielong_Text.delete(1.0, END)
        text_var = StringVar()
        self.entry1=Entry(self,width=50,font=('黑体', 15, 'bold'),state = 'readonly',textvariable = text_var).grid(row=4, column=1, columnspan=10)
        text_var.set('【简单模式】请输入一句诗歌：')
        self.entry2=Entry(self, width=50,font=('黑体', 15, 'bold'),textvariable=self.enter).grid(row=5, column=1, columnspan=10)
        Button(self,text='输入',font=('黑体', 15, 'bold'),command=self.change_state1).grid(row=5,column=7)
    def print2(self):
        self.jielong_Text.delete(1.0, END)
        text_var = StringVar()
        # Label(self, text='请输入诗词：', font=('黑体', 15, 'bold')).grid(row=2, column=2, stick=W)
        self.entry1=Entry(self,width=50,font=('黑体', 15, 'bold'),state = 'readonly',textvariable = text_var).grid(row=4, column=1, columnspan=10)
        text_var.set('【普通模式】请输入一句诗歌：')
        self.entry2=Entry(self, width=50,font=('黑体', 15, 'bold'),textvariable=self.enter).grid(row=5, column=1, columnspan=10)
        Button(self,text='输入',font=('黑体', 15, 'bold'),command=self.change_state2).grid(row=5,column=7)
    def print3(self):
        self.jielong_Text.delete(1.0, END)
        text_var = StringVar()
        self.entry1=Entry(self,width=50,font=('黑体', 15, 'bold'),state = 'readonly',textvariable = text_var).grid(row=4, column=1, columnspan=10)
        text_var.set('【困难模式】请输入一句诗歌：')
        self.entry2=Entry(self, width=50,font=('黑体', 15, 'bold'),textvariable=self.enter).grid(row=5, column=1, columnspan=10)
        Button(self,text='完成',font=('黑体', 15, 'bold'),command=self.change_state3).grid(row=5,column=7)

    def change_state1(self):
        enterstr = self.enter.get()
        self.enter.set("")
        self.write_to_Text('【简单模式】你的输入：' + enterstr + '\n')
        while enterstr != 'exit':
            data = test1.jielong_easy(enterstr)
            if data != 'false':
                self.write_to_Text('【简单模式】机器回复：'+data+'\n'+'【简单模式】机器回复：该你接啦！'+'\n'+'\n')
                play(data)

            else:
                self.write_to_Text('被你打败啦，我没办法接这句诗。'+'\n')
            break
    def change_state2(self):
        enterstr = self.enter.get()
        self.write_to_Text('【普通模式】你的输入：' + enterstr + '\n')
        self.enter.set("")
        while enterstr != 'exit':
            data = test2.jielong_normal(enterstr)
            if data != 'false':
                self.write_to_Text('【普通模式】机器回复：' + data + '\n' + '【普通模式】机器回复：该你接啦！' + '\n' + '\n')
                play(data)
            else:
                self.write_to_Text('被你打败啦，我没办法接这句诗。' + '\n')
            break
    def change_state3(self):
        enterstr = self.enter.get()
        self.enter.set("")
        self.write_to_Text('【困难模式】你的输入：' + enterstr + '\n')
        while enterstr != 'exit':
            data = test3.jielong_hard(enterstr)
            if data != 'false':
                self.write_to_Text('【困难模式】机器回复：' + data + '\n' + '【困难模式】机器回复：该你接啦！' + '\n' + '\n')
                play(data)

            else:
                self.write_to_Text('被你打败啦，我没办法接这句诗。' + '\n')
            break

    def write_to_Text(self,logmsg):
        logmsg_in = str(logmsg)       #换行
        self.jielong_Text.insert(END, logmsg_in)


# ******************词云*****************
class CiyunFrame(Frame):  # 继承Frame类
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.root = master  # 定义内部变量root
        self.createPage()
        self.btvalue = StringVar()

    def createPage(self):
        Label(self,text='诗歌词云', font=('黑体', 20, 'bold')).grid(row=0, column=6)
        Label(self,text='单词词云：', font=('黑体', 10, 'bold')).grid(row=1, column=1,columnspan=4)
        Label(self, text='多词词云：', font=('黑体', 10, 'bold')).grid(row=1, column=8,columnspan=4)
        Button(self,text='白居易',command=self.show1, height=6,bg="CornflowerBlue",relief=RAISED,activebackground="DarkBlue",font=('黑体', 10, 'bold'),width=7).grid(row=2,column=1,stick=W)
        Button(self, text='刘禹锡', command=self.show2,height=6,bg="CornflowerBlue",relief=RAISED,activebackground="DarkBlue", font=('黑体', 10, 'bold'),width=7).grid(row=2, column=2,stick=W)
        Button(self, text='李商隐', command=self.show3,height=6,bg="CornflowerBlue",relief=RAISED, activebackground="DarkBlue",font=('黑体', 10, 'bold'),width=7).grid(row=2, column=3,stick=W)
        Button(self, text='李清照', command=self.show4, height=6,bg="CornflowerBlue",relief=RAISED,activebackground="DarkBlue",font=('黑体', 10, 'bold'),width=7).grid(row=2, column=4,stick=W)
        Button(self, text='李白', command=self.show5, height=6,bg="CornflowerBlue",relief=RAISED,activebackground="DarkBlue",font=('黑体', 10, 'bold'),width=7).grid(row=3, column=1,stick=W)
        Button(self, text='杜甫', command=self.show6, height=6,bg="CornflowerBlue",relief=RAISED,activebackground="DarkBlue",font=('黑体', 10, 'bold'),width=7).grid(row=3, column=2,stick=W)
        Button(self, text='王维', command=self.show7, height=6,bg="CornflowerBlue",relief=RAISED,activebackground="DarkBlue",font=('黑体', 10, 'bold'),width=7).grid(row=3, column=3,stick=W)
        Button(self, text='苏轼', command=self.show8, height=6,bg="CornflowerBlue",relief=RAISED,activebackground="DarkBlue",font=('黑体', 10, 'bold'),width=7).grid(row=3, column=4,stick=W)
        Button(self, text='陆游', command=self.show9, height=6,bg="CornflowerBlue",relief=RAISED,activebackground="DarkBlue",font=('黑体', 10, 'bold'),width=7).grid(row=4, column=1,stick=W)
        Button(self, text='辛弃疾', command=self.show10,height=6,bg="CornflowerBlue",relief=RAISED, activebackground="DarkBlue",font=('黑体', 10, 'bold'),width=7).grid(row=4, column=2,stick=W)
        Button(self, text='纳兰性德', command=self.show11, height=6,bg="CornflowerBlue",relief=RAISED,activebackground="DarkBlue",font=('黑体', 10, 'bold'),width=7).grid(row=4, column=3,stick=W)
        Button(self, text='孟浩然', command=self.show12,height=6,bg="CornflowerBlue",relief=RAISED,activebackground="DarkBlue", font=('黑体', 10, 'bold'),width=7).grid(row=4, column=4,stick=W)
        Button(self, text='唐代', command=self.show13, height=6,bg="CornflowerBlue",relief=RAISED,activebackground="DarkBlue",font=('黑体', 10, 'bold'),width=7).grid(row=5, column=1,stick=W)
        Button(self, text='宋代', command=self.show14,height=6,bg="CornflowerBlue",relief=RAISED, activebackground="DarkBlue",font=('黑体', 10, 'bold'),width=7).grid(row=5, column=2,stick=W)
        Button(self, text='其他朝代', command=self.show15, height=6,bg="CornflowerBlue",relief=RAISED,activebackground="DarkBlue",font=('黑体', 10, 'bold'),width=7).grid(row=5, column=3,stick=W)
        Button(self, text='所有诗歌', command=self.show16, height=6,bg="CornflowerBlue",relief=RAISED,activebackground="DarkBlue",font=('黑体', 10, 'bold'),width=7).grid(row=5, column=4,stick=W)
        # Button(self, text='所有诗歌', command=self.show, font=('黑体', 10, 'bold'),width=6).grid(row=6, column=1)

        Button(self,text="白居易",command=self.show17,height=6,bg="#B0E0E6",relief=RAISED,font=('黑体', 10, 'bold'),width=7).grid(row=2,column=8,stick=W)
        # Button(self,text='白居易',command=self.show17, font=('黑体', 10, 'bold'),width=6).grid(row=2,column=8,stick=W)
        Button(self, text='刘禹锡', command=self.show18,height=6,bg="#B0E0E6",relief=RAISED, font=('黑体', 10, 'bold'),width=7).grid(row=2, column=9,stick=W)
        Button(self, text='李商隐', command=self.show19, height=6,bg="#B0E0E6",relief=RAISED,font=('黑体', 10, 'bold'),width=7).grid(row=2, column=10,stick=W)
        Button(self, text='李清照', command=self.show20,height=6,bg="#B0E0E6",relief=RAISED, font=('黑体', 10, 'bold'),width=7).grid(row=2, column=11,stick=W)
        Button(self, text='李白', command=self.show21, height=6,bg="#B0E0E6",relief=RAISED,font=('黑体', 10, 'bold'),width=7).grid(row=3, column=8,stick=W)
        Button(self, text='杜甫', command=self.show22,height=6,bg="#B0E0E6",relief=RAISED, font=('黑体', 10, 'bold'),width=7).grid(row=3, column=9,stick=W)
        Button(self, text='王维', command=self.show23, height=6,bg="#B0E0E6",relief=RAISED,font=('黑体', 10, 'bold'),width=7).grid(row=3, column=10,stick=W)
        Button(self, text='苏轼', command=self.show24, height=6,bg="#B0E0E6",relief=RAISED,font=('黑体', 10, 'bold'),width=7).grid(row=3, column=11,stick=W)
        Button(self, text='陆游', command=self.show25, height=6,bg="#B0E0E6",relief=RAISED,font=('黑体', 10, 'bold'),width=7).grid(row=4, column=8,stick=W)
        Button(self, text='辛弃疾', command=self.show26,height=6,bg="#B0E0E6",relief=RAISED, font=('黑体', 10, 'bold'),width=7).grid(row=4, column=9,stick=W)
        Button(self, text='纳兰性德', command=self.show27, height=6,bg="#B0E0E6",relief=RAISED,font=('黑体', 10, 'bold'),width=7).grid(row=4, column=10,stick=W)
        Button(self, text='孟浩然', command=self.show28,height=6,bg="#B0E0E6",relief=RAISED, font=('黑体', 10, 'bold'),width=7).grid(row=4, column=11,stick=W)
        Button(self, text='唐代', command=self.show29,height=6,bg="#B0E0E6",relief=RAISED, font=('黑体', 10, 'bold'),width=7).grid(row=5, column=8,stick=W)
        Button(self, text='宋代', command=self.show30,height=6,bg="#B0E0E6",relief=RAISED, font=('黑体', 10, 'bold'),width=7).grid(row=5, column=9,stick=W)
        Button(self, text='其他朝代', command=self.show31,height=6,bg="#B0E0E6",relief=RAISED, font=('黑体', 10, 'bold'),width=7).grid(row=5, column=10,stick=W)
        Button(self, text='所有诗歌', command=self.show32, height=6,bg="#B0E0E6",relief=RAISED,font=('黑体', 10, 'bold'),width=7).grid(row=5, column=11,stick=W)


    def show1(self):
        top1 = Toplevel()
        top1.title('白居易词云')
        global photo1
        global img1
        img1 = Image.open('./ciyun_single/白居易词云.png')  # 打开图片
        photo1 = ImageTk.PhotoImage(img1)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top1, image=photo1)
        imglabel.grid(row=6, columnspan=12)

        # global photo
        # global img
        # img = Image.open('../ciyun_single/白居易词云.png')  # 打开图片
        # photo = ImageTk.PhotoImage(img)  # 用PIL模块的PhotoImage打开
        # imglabel = Label(self, image=photo)
        # imglabel.grid(row=6, columnspan=12)
    def show2(self):
        top2 = Toplevel()
        top2.title('白居易词云')
        # text = self.baijuyi['text']
        # print(text)
        global photo2
        global img2
        img2 = Image.open('./ciyun_single/刘禹锡词云.png')  # 打开图片
        photo2 = ImageTk.PhotoImage(img2)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top2, image=photo2)
        imglabel.grid(row=6, columnspan=12)

    def show3(self):

        top3 = Toplevel()
        top3.title('李商隐词云')
        # text = self.baijuyi['text']
        # print(text)
        global photo3
        global img3
        img3 = Image.open('./ciyun_single/李商隐词云.png')  # 打开图片
        photo3 = ImageTk.PhotoImage(img3)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top3, image=photo3)
        imglabel.grid(row=6, columnspan=12)
    def show4(self):
        top4 = Toplevel()
        top4.title('李清照词云')
        # text = self.baijuyi['text']
        # print(text)
        global photo4
        global img4
        img4 = Image.open('./ciyun_single/李清照词云.png')  # 打开图片
        photo4 = ImageTk.PhotoImage(img4)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top4, image=photo4)
        imglabel.grid(row=6, columnspan=12)

        # global photo
        # global img
        # img = Image.open('../ciyun_single/李清照词云.png')  # 打开图片
        # photo = ImageTk.PhotoImage(img)  # 用PIL模块的PhotoImage打开
        # imglabel = Label(self, image=photo)
        # imglabel.grid(row=6, columnspan=12)
    def show5(self):
        top5 = Toplevel()
        top5.title('李白词云')
        # text = self.baijuyi['text']
        # print(text)
        global photo5
        global img5
        img5 = Image.open('./ciyun_single/李白词云.png')  # 打开图片
        photo5 = ImageTk.PhotoImage(img5)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top5, image=photo5)
        imglabel.grid(row=6, columnspan=12)

        # global photo
        # global img
        # img = Image.open('../ciyun_single/李白词云.png')  # 打开图片
        # photo = ImageTk.PhotoImage(img)  # 用PIL模块的PhotoImage打开
        # imglabel = Label(self, image=photo)
        # imglabel.grid(row=6, columnspan=12)
    def show6(self):
        top6 = Toplevel()
        top6.title('李白词云')
        # text = self.baijuyi['text']
        # print(text)
        global photo6
        global img6
        img6 = Image.open('./ciyun_single/杜甫词云.png')  # 打开图片
        photo6 = ImageTk.PhotoImage(img6)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top6, image=photo6)
        imglabel.grid(row=6, columnspan=12)

        # global photo
        # global img
        # img7 = Image.open('../ciyun_single/杜甫词云.png')  # 打开图片
        # photo = ImageTk.PhotoImage(img)  # 用PIL模块的PhotoImage打开
        # imglabel = Label(self, image=photo)
        # imglabel.grid(row=6, columnspan=12)
    def show7(self):
        top7 = Toplevel()
        top7.title('王维词云')
        # text = self.baijuyi['text']
        # print(text)
        global photo7
        global img7
        img7 = Image.open('./ciyun_single/王维词云.png')  # 打开图片
        photo7 = ImageTk.PhotoImage(img7)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top7, image=photo7)
        imglabel.grid(row=6, columnspan=12)

        # global photo
        # global img
        # img = Image.open('../ciyun_single/王维词云.png')  # 打开图片
        # photo = ImageTk.PhotoImage(img)  # 用PIL模块的PhotoImage打开
        # imglabel = Label(self, image=photo)
        # imglabel.grid(row=6, columnspan=12)
    def show8(self):
        top8 = Toplevel()
        top8.title('苏轼词云')
        # text = self.baijuyi['text']
        # print(text)
        global photo8
        global img8
        img8 = Image.open('./ciyun_single/苏轼词云.png')  # 打开图片
        photo8 = ImageTk.PhotoImage(img8)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top8, image=photo8)
        imglabel.grid(row=6, columnspan=12)
        #
        # global photo
        # global img
        # img = Image.open('../ciyun_single/苏轼词云.png')  # 打开图片
        # photo = ImageTk.PhotoImage(img)  # 用PIL模块的PhotoImage打开
        # imglabel = Label(self, image=photo)
        # imglabel.grid(row=6, columnspan=12)
    def show9(self):
        top9 = Toplevel()
        top9.title('陆游词云')
        global photo9
        global img9
        img9 = Image.open('./ciyun_single/陆游词云.png')  # 打开图片
        photo9 = ImageTk.PhotoImage(img9)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top9, image=photo9)
        imglabel.grid(row=6, columnspan=12)
    def show10(self):
        top10 = Toplevel()
        top10.title('辛弃疾词云')
        global photo10
        global img10
        img10 = Image.open('./ciyun_single/辛弃疾词云.png')  # 打开图片
        photo10 = ImageTk.PhotoImage(img10)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top10, image=photo10)
        imglabel.grid(row=6, columnspan=12)
    def show11(self):
        top11 = Toplevel()
        top11.title('纳兰性德词云')
        global photo11
        global img11
        img11 = Image.open('./ciyun_single/纳兰性德词云.png')  # 打开图片
        photo11 = ImageTk.PhotoImage(img11)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top11, image=photo11)
        imglabel.grid(row=6, columnspan=12)
    def show12(self):
        top12 = Toplevel()
        top12.title('孟浩然词云')
        global photo12
        global img12
        img12 = Image.open('./ciyun_single/孟浩然词云.png')  # 打开图片
        photo12 = ImageTk.PhotoImage(img12)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top12, image=photo12)
        imglabel.grid(row=6, columnspan=12)
    def show13(self):
        top13 = Toplevel()
        top13.title('唐代词云')
        global photo13
        global img13
        img13 = Image.open('./ciyun_single/唐代词云.png')  # 打开图片
        photo13 = ImageTk.PhotoImage(img13)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top13, image=photo13)
        imglabel.grid(row=6, columnspan=12)
    def show14(self):
        top14 = Toplevel()
        top14.title('宋代词云')
        global photo14
        global img14
        img14 = Image.open('./ciyun_single/宋代词云.png')  # 打开图片
        photo14 = ImageTk.PhotoImage(img14)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top14, image=photo14)
        imglabel.grid(row=6, columnspan=12)
    def show15(self):
        top15 = Toplevel()
        top15.title('其他朝代词云')
        global photo15
        global img15
        img15 = Image.open('./ciyun_single/其他朝代词云.png')  # 打开图片
        photo15 = ImageTk.PhotoImage(img15)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top15, image=photo15)
        imglabel.grid(row=6, columnspan=12)
    def show16(self):
        top16 = Toplevel()
        top16.title('所有诗歌词云')
        global photo16
        global img16
        img16 = Image.open('./ciyun_single/所有诗歌词云.png')  # 打开图片
        photo16 = ImageTk.PhotoImage(img16)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top16, image=photo16)
        imglabel.grid(row=6, columnspan=12)

    def show17(self):
        top17 = Toplevel()
        top17.title('白居易多字词云')
        global photo17
        global img17
        img17 = Image.open('./ciyun_double/白居易两词词云.jpg')  # 打开图片
        photo17 = ImageTk.PhotoImage(img17)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top17, image=photo17)
        imglabel.grid(row=6, columnspan=12)

    def show18(self):
        top18 = Toplevel()
        top18.title('刘禹锡多字词云')
        global photo18
        global img18
        img18 = Image.open('./ciyun_double/刘禹锡两词词云.jpg')  # 打开图片
        photo18 = ImageTk.PhotoImage(img18)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top18, image=photo18)
        imglabel.grid(row=6, columnspan=12)
    def show19(self):
        top19 = Toplevel()
        top19.title('李商隐多字词云')
        global photo19
        global img19
        img19 = Image.open('./ciyun_double/李商隐两词词云.jpg')  # 打开图片
        photo19 = ImageTk.PhotoImage(img19)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top19, image=photo19)
        imglabel.grid(row=6, columnspan=12)
    def show20(self):
        top20 = Toplevel()
        top20.title('李清照多字词云')
        global photo20
        global img20
        img20 = Image.open('./ciyun_double/李清照两词词云.jpg')  # 打开图片
        photo20 = ImageTk.PhotoImage(img20)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top20, image=photo20)
        imglabel.grid(row=6, columnspan=12)
    def show21(self):
        top21 = Toplevel()
        top21.title('李白多字词云')
        global photo21
        global img21
        img21 = Image.open('./ciyun_double/李白两词词云.jpg')  # 打开图片
        photo21 = ImageTk.PhotoImage(img21)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top21, image=photo21)
        imglabel.grid(row=6, columnspan=12)
    def show22(self):
        top22 = Toplevel()
        top22.title('杜甫多字词云')
        global photo22
        global img22
        img22 = Image.open('./ciyun_double/杜甫两词词云.jpg')  # 打开图片
        photo22 = ImageTk.PhotoImage(img22)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top22, image=photo22)
        imglabel.grid(row=6, columnspan=12)
    def show23(self):
        top23 = Toplevel()
        top23.title('王维多字词云')
        global photo23
        global img23
        img23 = Image.open('./ciyun_double/王维两词词云.jpg')  # 打开图片
        photo23 = ImageTk.PhotoImage(img23)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top23, image=photo23)
        imglabel.grid(row=6, columnspan=12)
    def show24(self):
        top24 = Toplevel()
        top24.title('苏轼多字词云')
        global photo24
        global img24
        img24 = Image.open('./ciyun_double/苏轼两词词云.jpg')  # 打开图片
        photo24 = ImageTk.PhotoImage(img24)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top24, image=photo24)
        imglabel.grid(row=6, columnspan=12)
    def show25(self):
        top25 = Toplevel()
        top25.title('陆游多字词云')
        global photo25
        global img25
        img25 = Image.open('./ciyun_double/陆游两词词云.jpg')  # 打开图片
        photo25 = ImageTk.PhotoImage(img25)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top25, image=photo25)
        imglabel.grid(row=6, columnspan=12)
    def show26(self):
        top26 = Toplevel()
        top26.title('辛弃疾多字词云')
        global photo26
        global img26
        img26 = Image.open('./ciyun_double/辛弃疾两词词云.jpg')  # 打开图片
        photo26 = ImageTk.PhotoImage(img26)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top26, image=photo26)
        imglabel.grid(row=6, columnspan=12)
    def show27(self):
        top27 = Toplevel()
        top27.title('纳兰性德多字词云')
        global photo27
        global img27
        img27 = Image.open('./ciyun_double/纳兰性德两词词云.jpg')  # 打开图片
        photo27 = ImageTk.PhotoImage(img27)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top27, image=photo27)
        imglabel.grid(row=6, columnspan=12)
    def show28(self):
        top28 = Toplevel()
        top28.title('孟浩然多字词云')
        global photo28
        global img28
        img28 = Image.open('./ciyun_double/孟浩然两词词云.jpg')  # 打开图片
        photo28 = ImageTk.PhotoImage(img28)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top28, image=photo28)
        imglabel.grid(row=6, columnspan=12)
    def show29(self):
        top29 = Toplevel()
        top29.title('唐代多字词云')
        global photo29
        global img29
        img29 = Image.open('./ciyun_double/唐代两词词云.jpg')  # 打开图片
        photo29 = ImageTk.PhotoImage(img29)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top29, image=photo29)
        imglabel.grid(row=6, columnspan=12)
    def show30(self):
        top30 = Toplevel()
        top30.title('宋代多字词云')
        global photo30
        global img30
        img30 = Image.open('./ciyun_double/宋代两词词云.jpg')  # 打开图片
        photo30 = ImageTk.PhotoImage(img30)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top30, image=photo30)
        imglabel.grid(row=6, columnspan=12)
    def show31(self):
        top31 = Toplevel()
        top31.title('其他朝代多字词云')
        global photo31
        global img31
        img31 = Image.open('./ciyun_double/其他朝代两词词云.jpg')  # 打开图片
        photo31 = ImageTk.PhotoImage(img31)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top31, image=photo31)
        imglabel.grid(row=6, columnspan=12)
    def show32(self):
        top32 = Toplevel()
        top32.title('所有诗歌多字词云')
        global photo32
        global img32
        img32 = Image.open('./ciyun_double/所有诗歌两词词云.jpg')  # 打开图片
        photo32 = ImageTk.PhotoImage(img32)  # 用PIL模块的PhotoImage打开
        imglabel = Label(top32, image=photo32)
        imglabel.grid(row=6, columnspan=12)



