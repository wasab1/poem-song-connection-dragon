import re
import pickle
from pypinyin import pinyin
from pypinyin import lazy_pinyin
from collections import defaultdict

with open('./poem.txt', 'r', encoding='utf-8') as f:  # 通过读的方式打开poem.txt文件
    lines = f.readlines()  # 逐行读取
    f.close()
news = []
for line in lines: # 逐行遍历,去掉解释等
	contents =re.findall(r' \'contents\': \'(.*?)\'',line,re.DOTALL)#一起爬到的用这个，得到内容
	for content in contents:
		str = "".join(content)
		print(content)
		news.append(str)


sents = []#创建一个空的发送列表
for new in news:#通过for循环的方式获取文件中读取的数据
	parts = re.findall(r'[\s\S]*?[。？！]', new.strip())#通过正则表达式匹配相应的字符"。 ？ ！"，并将其去掉
	for part in parts:
		if len(part) >= 5:#如果获取诗句文字部分超过5个字符，则将其写入sents列表
			sents.append(part)
poem_dict = defaultdict(list)
i = 1
for sent in sents:
	print(sent)#通过循环打印的方式输出诗句
	str = "".join(sent[0])
	# head = sent[0]  # 这是困难模式的字典
	# head = pinyin(str)  # 这是普通模式的字典
	head = lazy_pinyin(str) # 这是简单模式的字典
	data = "".join(sent)
	print(head,i)
	i = i + 1
	# poem_dict[head].append(data)  # 这是困难模式的字典
	realhead = head[0]
	poem_dict[realhead].append(data)  # 这是简单模式的字典
	# poem_dict[realhead[0]].append(data)  # 这是普通模式的字典
with open('easy_poemDict.pk', 'wb') as f:
	pickle.dump(poem_dict, f)#此处通过序列化实现将poem_dict列表写入到poemDict.pk文件中
	f.close()




